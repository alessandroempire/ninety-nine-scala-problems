package tests

import org.junit.runner.RunWith
import org.scalatest.FunSpec
import org.scalatest.junit.JUnitRunner


@RunWith(classOf[JUnitRunner])
class ListsTests extends FunSpec {
  
  describe("99 Problems of list") {
    
    it("P01 should return the last element of a list"){
        assert(org.alessandro.scalaproblems.Lists.last(List(1, 1, 2, 3, 5, 8)) == 8)
    }
    
    it("P02 should return the last but one element of a list"){
      assert(org.alessandro.scalaproblems.Lists.penultimate(List(1, 1, 2, 3, 5, 8)) == 5)
    }
    
    it("P03 should find the Kth element of a list."){
      assert(org.alessandro.scalaproblems.Lists.nth(2, List(1, 1, 2, 3, 5, 8)) == 2)
    }
    
    it("P04 should Find the number of elements of a list."){
      assert(org.alessandro.scalaproblems.Lists.length(List(1, 1, 2, 3, 5, 8)) == 6)
    }
    
    it("P05 Reverse a list."){
      assert(org.alessandro.scalaproblems.Lists.reverse(List(1, 1, 2, 3, 5, 8)) == List(8, 5, 3, 2, 1, 1))
    }
    
    it("P06 Find out whether a list is a palindrome."){
      assert(org.alessandro.scalaproblems.Lists.isPalindrome(List(1, 2, 3, 2, 1)) == true)
    }
    
    it("P07 Flatten a nested list structure."){
      assert(org.alessandro.scalaproblems.Lists.flatten(List(List(1, 1), 2, List(3, List(5, 8)))) == List(1, 1, 2, 3, 5, 8))
    }
    
    it("P08 Eliminate consecutive duplicates of list elements."){
      assert(org.alessandro.scalaproblems.Lists.compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) == List('a, 'b, 'c, 'a, 'd, 'e))
    }
    
    it("P09 Pack consecutive duplicates of list elements into sublists."){
      assert(org.alessandro.scalaproblems.Lists.pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) 
          == List(List('a, 'a, 'a, 'a), List('b), List('c, 'c), List('a, 'a), List('d), List('e, 'e, 'e, 'e)) )
    }
    
    it("P10 Run-length encoding of a list."){
      assert(org.alessandro.scalaproblems.Lists.encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) 
          == List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e)) )
    }
    
    it("P12 Run-length encoding of a list."){
      assert(org.alessandro.scalaproblems.Lists.decode(List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e) )) 
          == List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e) )
    }
    
    it("P14 Duplicate the elements of a list."){
      assert(org.alessandro.scalaproblems.Lists.duplicate(List('a, 'b, 'c, 'c, 'd)) 
          == List('a, 'a, 'b, 'b, 'c, 'c, 'c, 'c, 'd, 'd) )
    }
    
    it("P15 Duplicate the elements of a list a given number of times."){
      assert(org.alessandro.scalaproblems.Lists.duplicateN(3, List('a, 'b, 'c, 'c, 'd)) 
          == List('a, 'a, 'a, 'b, 'b, 'b, 'c, 'c, 'c, 'c, 'c, 'c, 'd, 'd, 'd))

    }
    
  }
  
}