package org.alessandro.scalaproblems

/**
 * The Class Lists.
 */
object Lists {

  def last(list: List[Any]): Any = {
    list.foldLeft(list.head)((x, y) => y)
  }

  def penultimate(list: List[Any]): Any = {
    list.foldLeft((list.head, list.tail.head))({ case ((h, t), v) => (t, v) })._1
  }

  def nth(pos: Int, list: List[Any]): Any = {
    (pos, list) match {
      case (0, h :: _)             => h
      case (k, _ :: tail) if k > 0 => nth(k - 1, tail)
      case _                       => throw new NoSuchElementException
    }
  }

  def length(list: List[Any]): Int = {
    list.foldLeft(0)((x, _) => x + 1)
  }

  def reverse[A](list: List[A]): List[A] = {
    list.foldLeft(List[A]())((x, y) => y :: x)
  }

  def isPalindrome[A](list: List[A]): Boolean = {
    list match {
      case Nil     => true
      case List(a) => true
      case list    => (list.head == list.last && isPalindrome(list.tail.init))
    }
  }

  def flatten(list: List[Any]): List[Any] = {
    list match {
      case Nil                  => Nil
      case (h: List[_]) :: tail => flatten(h) ::: flatten(tail)
      case h :: tail            => h :: flatten(tail)
    }
  }

  def compress[A](list: List[A]): List[A] = {
    list.foldLeft(List[A]())({
      case (List(), e)           => List(e)
      case (l, e) if l.head == e => l
      case (l, e) if l.head != e => e :: l
    }).reverse
  }

  def pack[A](list: List[Any]) = {
    val tl = list.foldLeft( ( List[Any](), List[Any]() )  ) (
      {
        case ((l, List()), e) => (l, List(e))
        case ((l, l1) , e) if l1.head == e => (l, e :: l1)
        case ((l, l1) , e) if l1.head != e => (l1 :: l, List(e))
      })
      (tl._2 :: tl._1).reverse
  }
  
  def encode[A](list: List[Any]) = {
    if (list.isEmpty){
      List[Any]()
    } else {
      val tl = list.foldLeft( ( List[Any](), (0, list.head))  ) (
      {
        case ((l, (0, _)), e) => (l, (1, e))
        case ((l, (acc, l1)) , e) if l1 == e => (l, (acc+1, l1))
        case ((l, (acc, l1)) , e) if l1 != e => ( (acc, l1) :: l, (1, e))
      })
      (tl._2 :: tl._1).reverse  
    }
  }
  
  def decode[A](list: List[(Int,Any)]): List[Any] = {
    if (list.isEmpty){
      List[Any]()
    } else {
      list.foldLeft( List[Any]() )( 
          {case (l, (n,c)) => l ++ List.fill(n)(c) })
    }
  }
  
  def duplicate[A](list: List[Any]) = {
    list.foldLeft( List[Any]() )( (l, e) =>  e :: (e :: l)).reverse
  }
  
  def duplicateN [A](n : Int, list: List[Any]): List[Any] = {
    
    def myDuplicateN[A](n:Int, list: List[Any], listAcc: List[Any] , nacc : Int): List[Any] = {
      list match {
        case Nil => listAcc
        case head :: tail => 
            if (nacc == 0 ) {
              myDuplicateN(n, tail, listAcc, n)
            } else {
              myDuplicateN(n, list, head :: listAcc, nacc-1)
            }
      }  
    }
    
    myDuplicateN(n, list, List(), n).reverse
  }
  

  def main(args: Array[String]) = {
    val l = org.alessandro.scalaproblems.Lists.duplicateN(3, List('a, 'b, 'c, 'c, 'd))
    println(l)
  }

}